import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    currentUser: null,
    error: null,
    loading: false
}

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        //when the sign in process start (when youn press submit) the error will be null and loading will be true and there is no user yet
        signInStart: (state) => {
            state.error= null;
            state.loading= true;
        },
       //when the sign in process success (correct user) the current user data will be the payload data the error will be null and loading will be false
        signInSuccess: (state, action) => {
            state.currentUser = action.payload;
            state.error= null;
            state.loading= false;
        },
        //when the sign in process fail (incorrect user) the error will be the payload data and loading will be false
        signInFailure: (state, action) => {
            state.error= action.payload;
            state.loading= false;
        },
        //when the update process start (when youn press submit) the error will be null and loading will be true and there is no user yet
        updateStart: (state) => {
            state.error= null;
            state.loading= true;
        },
       //when the update process success (correct user) the current user data will be the payload data the error will be null and loading will be false
        updateSuccess: (state, action) => {
            state.currentUser = action.payload;
            state.error= null;
            state.loading= false;
        },
        //when the update process fail (incorrect user) the error will be the payload data and loading will be false
        updateFailure: (state, action) => {
            state.error= action.payload;
            state.loading= false;
        },
        //when the delete user process start (when youn press submit) the error will be null and loading will be true and there is no user yet
        deleteUserStart: (state) => {
            state.error= null;
            state.loading= true;
        },
       //when the delete user process success (correct user) the current user will be null the error will be null and loading will be false
       deleteUserSuccess: (state) => {
            state.currentUser = null;
            state.error= null;
            state.loading= false;
        },
        //when the delete user  process fail (incorrect user) the error will be the payload data and loading will be false
        deleteUserFailure: (state, action) => {
            state.error= action.payload;
            state.loading= false;
        },
        signoutSuccess: (state) => {
            state.currentUser = null;
            state.error= null;
            state.loading= false;
        },
    }
});

export const { signInStart, signInSuccess, signInFailure, updateStart, updateSuccess, updateFailure, deleteUserStart, deleteUserSuccess, deleteUserFailure, signoutSuccess} = userSlice.actions;

const  userReduser = userSlice.reducer;

export default userReduser;