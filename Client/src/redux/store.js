import { configureStore, combineReducers} from '@reduxjs/toolkit'
import userReduser from './user/userSlice'
import themeReducer from './theme/themeSlice'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import persistStore from 'redux-persist/es/persistStore';

//1. define the root reducer which include all our reducers
const rootReducer = combineReducers({
    user: userReduser,
    theme: themeReducer
});

//2. define the persist Configuration
const persistConfig = {
    key: 'root',
    storage,
    version: 1
}
//3. define the persisted Reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);

//4. export our store reducer & middleware
export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false})
})

//5. export persist store
export const persistore = persistStore(store);
