import React from 'react'
import logo from '../assets/logo.png';

export default function About() {
  return (
    <div className='min-h-screen flex items-center justify-center'>
      <div className='flex-col max-w-3xl mx-auto p-3 text-center'>
        <div>
          <h1 className='text-3xl font-semibold my-7'> About NaBlog</h1>
          <div className='text-md text-gray-500 flex flex-col gap-6'>
            <p> Welcome to NaBlog! this blog was created by Nadia Qwaider as a personal project to share her thoughts and ideas with the world. Nadia is a web and content developer who loves to write about technology, coding, learning and everything in between</p>
          </div>
        </div>
      </div>
      <div className='flex-col max-w-3xl mx-auto p-3 text-center'><img src={logo} /></div>
    </div>
  )
}
