import React from 'react'
import CallToAction from '../components/CallToAction'
export default function Projects() {
  return (
    <div className='min-h-screen  mx-auto flex justify-center items-center flex-col gap-6 p-3'>
      <h1 className='text-3xl font-semibold'>
        Projects
      </h1>
      <p className='text-md text-gray-500'>Build fun and engaging projects while learning HTML, CSS and Javascript</p>
      <div className='p-3 bg-amber-100 dark:bg-gray-800'><CallToAction/></div>    </div>
  )
}
