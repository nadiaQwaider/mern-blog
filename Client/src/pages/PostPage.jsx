import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom' 
import { Button, Spinner} from 'flowbite-react'
import CallToAction from '../components/CallToAction';
import CommentSection from '../components/CommentSection';
import PostCard from '../components/PostCard';
export default function PostPage() {
    const {postSlug} = useParams();
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [post, setPost] = useState([]);
    const [recentPosts, setRecentPosts] = useState(null);
    useEffect(()=>{
        const fetchpost = async() => {
            try{
             const res = await fetch (`/api/post/get-posts?slug=${postSlug}`);
             const data = await res.json();
             if (!res.ok) {
                setLoading(false);
                setError(true);
                return;
             }
             if (res.ok){
               setLoading(false);
               setError(false);
               setPost(data.posts[0]);
               
             }
            } catch(error){
               setError(true);
               setLoading(false);
            }
        }
        fetchpost();
    },[postSlug]);

    //function to fetch recent posts
      useEffect(() => {
        try{
        const fetchRecentPosts = async () => {
        const res = await fetch (`/api/post/get-posts?limit=3`);
        const data = await res.json();
        if(res.ok){
            setRecentPosts(data.posts)
        }
        }
        fetchRecentPosts();
        }catch(error){
         console.log(error.message);
        }
      }, [])
    if (loading === true) return ( <div className='flex justify-center items-center min-h-screen'><Spinner size='xl'/></div>);
    return (
        <main className='p-3 flex flex-col max-w-6xl mx-auto min-h-screen'>
            <h1 className='text-3xl mt-10 p-3 text-center font-serif max-w-2xl mx-auto lg:text-4xl'>{post && post.title}</h1>
            <Link to={`/search?category=${post && post.category}`} className='flex justify-center items-center p-3'> 
                <Button color='gray' pill sixe='xs'>{post && post.category}</Button>
            </Link>
            <img src = {post && post.image} alt={post && post.title} className='mt-10 p-3 max-h-[500px] object-cover'/>
            <div className='flex justify-between p-3 border-b border-slate-500 mx-auto w-full max-w-2xl text-xs'>
                <span>{post && new Date(post.createdAt).toLocaleDateString()}</span>
                <span className='italic'>{post && (post.content.length / 1000).toFixed(0)} mins read</span>
            </div>
            <div className='p-3 max-w-2xl mx-auto w-full overflow-hidden' dangerouslySetInnerHTML={{__html: post && post.content}}></div>
            <hr/>
            <div className='p-3 max-w-4xl mx-auto w-full'>
                <CallToAction />
            </div>
            <CommentSection postId={post && post._id} />
            <div className='flex flex-col justify-center items-center mb-5 mt-5'>
                <h1 className='text-3xl mt-10 p-3 text-center font-serif max-w-2xl mx-auto lg:text-4xl'>Recent Articles</h1>
                <div className='flex flex-wrap gap-2 mt-5'>
                { recentPosts &&
                   recentPosts.map((post) => (
                    <PostCard key={post._id} post={post}/>
                   ))}
                </div>
                
            </div>
        </main>
    ) 
}
