// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: import.meta.env.VITE_FIREBASE_API_KEY,
  authDomain: "mern-blog-ae402.firebaseapp.com",
  projectId: "mern-blog-ae402",
  storageBucket: "mern-blog-ae402.appspot.com",
  messagingSenderId: "580182855374",
  appId: "1:580182855374:web:3de3e20f7efff89f168a9f",
  measurementId: "G-CMGCW0315V"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);