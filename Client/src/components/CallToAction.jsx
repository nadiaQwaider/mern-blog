import { Button } from 'flowbite-react'
import React from 'react'

export default function 
() {
  return (
    <div className='w-full flex flex-col sm:flex-row p-3 border border-teal-500 justify-center items-center rounded-tl-3xl rounded br-3xl text-center '>
        <div className='flex-1 flex flex-col justify-center'>
            <h2 className='text-2xl'>
                Want to learn More about JavaScript?
            </h2>
            <p className='text-gray-500 my-2'>
                Checkout these resources with many javascripts projects
            </p>
            <Button gradientDuoTone='purpleToPink' className='rounded-tl-xl rounded-bl-none'>
                <a href='/projects'> JavaScript Projects</a>
            </Button>
        </div>
        <div className='flex-1 p-7'>
        <img src='https://cdni.iconscout.com/illustration/premium/thumb/javascript-developer-working-using-big-screen-4190671-3491175.png' />
        </div>
        
    </div>
  )
}
