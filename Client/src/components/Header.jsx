import { Button, Navbar, TextInput, Dropdown, Avatar} from 'flowbite-react'
import React, { useEffect, useState } from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import {AiOutlineSearch} from 'react-icons/ai'
import {FaMoon, FaSun} from 'react-icons/fa'
import { useSelector, useDispatch } from 'react-redux'
import { toggleTheme } from '../redux/theme/themeSlice'
import { signoutSuccess } from '../redux/user/userSlice'
import logo from '../assets/logo.png';

export default function Header() {
    const path = useLocation().pathname;
    const location = useLocation();
    const navigate = useNavigate()
    const {currentUser} = useSelector(state => state.user)
    const dispatch = useDispatch();
    const {theme} = useSelector(state => state.theme)
    const [searchTerm, setSearchTerm] = useState('');
      
    // header methods

    //extract searchTerm from URL
    useEffect(() => {
        const urlParams = new URLSearchParams(location.search);
        const searchTermFromUrl = urlParams.get('searchTerm');
        if (searchTermFromUrl){
            setSearchTerm(searchTermFromUrl);
        } 
    }, [location.search])

    //Sign out
    const handleSignout = async() => {
        try {
            const res = await fetch('/api/user/signout', {
                method: 'POST',})
            const data = await res.json();
            if (!res.ok){
                console.log(data.message)
            } else{
                dispatch(signoutSuccess())
            }
    
        } catch(error){
            console.log(error.message)
        }
    };
    // when Enter Search method change the url with it
    const handleSearch = (e) => {
        e.preventDefault();
        const urlParams = new URLSearchParams(location.search);
        urlParams.set('searchTerm', searchTerm);
        const searchQuery = urlParams.toString();
        navigate(`/search?${searchQuery}`);
    }

    //header content
  return (
    <Navbar className='border-b-2'>
        <Link to = "/" className=''>
             <img src={logo} className='h-20 px-2 py-1 rounded-full'/>
        </Link>

        <form onSubmit={handleSearch}>
            <TextInput
              type='text'
              placeholder='Search...'
              rightIcon={AiOutlineSearch}
              className='hidden lg:inline'
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}
              />
        </form>
        <Button className='w-12 h-10 lg:hidden' color='gray' pill> <AiOutlineSearch/></Button>
        <div className='flex gap-2 md:order-2'>
        <Button className='w-12 h-10 hidden sm:inline' color='gray' pill onClick={ () => dispatch(toggleTheme())}> 
             { (theme === "dark") ? (<FaMoon />) : (<FaSun />) }
        </Button>
        {currentUser ? ( 
                        <Dropdown arrowIcon={false} inline label= { <Avatar alt='user' img={currentUser.profilePicture} rounded /> } >
                            <Dropdown.Header>
                                <span className='block text-sm font-medium'>{currentUser.username}</span>
                                <span className='block text-sm font-medium'>{currentUser.email}</span>
                            </Dropdown.Header>
                                <Link to='/dashboard?tab=profile'>
                                    <Dropdown.Item>Profile</Dropdown.Item>
                                </Link>
                                <Dropdown.Divider/>
                                <Link to='/sign-in'>
                                    <Dropdown.Item onClick={handleSignout}>Sign Out</Dropdown.Item>
                                </Link>
                            </Dropdown>
                        ) : (
        
                        <Link to= '/sign-in'>
                            <Button gradientDuoTone= 'purpleToBlue' outline> Sign In</Button>
                        </Link>
                        ) }
        <Navbar.Toggle/>
        </div>
        <Navbar.Collapse>
            <Navbar.Link active={path === '/'} as={'div'}>
                <Link to='/'>Home</Link>
            </Navbar.Link>
            <Navbar.Link active={path === '/about'} as={'div'}>
                <Link to='/about'>About</Link>
            </Navbar.Link>
            <Navbar.Link active={path === '/projects'} as={'div'}>
                <Link to='/projects'>Projects</Link>
            </Navbar.Link>
        </Navbar.Collapse>

    </Navbar>
  )
}
