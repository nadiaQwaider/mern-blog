import { useEffect, useState } from "react";
import { useSelector } from "react-redux"
import {Table} from "flowbite-react"
import { Link } from "react-router-dom";
import 'tailwind-scrollbar'
import { Alert, Button, Modal, TextInput } from 'flowbite-react'
import { HiOutlineExclamationCircle } from 'react-icons/hi'
import {FaCheck, FaTimes} from 'react-icons/fa'

export default function DashUsers(){
    const {currentUser} = useSelector(state => state.user)
    const [users, setUsers] = useState([])
    const [showMore, setShowMore] = useState(true)
    const [showModal, setShowModal] = useState(false)
    const [userIdToDelete, setuserIdToDelete] = useState('')

    useEffect(() => {
        const fetchUsers = async () => {
            try{
             const res = await fetch(`/api/user/get-users`)
             const data = await res.json()
             if (res.ok){
                setUsers(data.users);
                if (data.users.length < 9){
                    setShowMore(false)
                }
             }
            } catch(error){
                console.log(error.message)
            }
        };
        if(currentUser.isAdmin){
            fetchUsers(); 
        }
    },[currentUser._id]);
    
    const handleShowMore = async() => {
        try{
            const startIndex = users.length;
            const res = await fetch(`/api/user/get-users?startIndex=${startIndex}`);
            const data = await res.json()
            if (res.ok){
               setUsers((prev) => [...prev, ...data.users]);
               if (data.users.length < 9){
                   setShowMore(false)
               }
            }
           } catch(error){
               console.log(error.message)
           }
    }

    const handleDeleteUser = async() => {
      setShowModal(false);
      try{
        const res = await fetch(`/api/user/delete/${userIdToDelete}`,{
            method:'DELETE'
        });
        const data = await res.json();
        if(!res.ok){
         console.log(data.message)
        } else {
            setUsers((prev) => prev.filter((user) => user._id !== userIdToDelete));
        }
      }catch(error){
        console.log(error.message)
    }
}
    return(
    <div className="table-auto overflow-x-scroll md:mx-auto p-3 scrollbar scrollbar-track-slate-100 scrollbar-thumb-slate-300 dark:scrollbar-track-slate-700 dark:scrollbar-thumb-slate-500 ">
    {currentUser.isAdmin === true && users.length > 0 ? (
    <>
    <Table hoverable className="shadow-md">
        <Table.Head>
            <Table.HeadCell>ID</Table.HeadCell>
            <Table.HeadCell>Created Date </Table.HeadCell>
            <Table.HeadCell>user Image</Table.HeadCell>
            <Table.HeadCell>user Name</Table.HeadCell>
            <Table.HeadCell>Email</Table.HeadCell>
            <Table.HeadCell>is Admin</Table.HeadCell>
            <Table.HeadCell>Delete</Table.HeadCell>
        </Table.Head>
        {users.map((user)=> (
            <Table.Body className="divide-y" key={user._id}>
            <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
            <Table.Cell>{user.startIndex}</Table.Cell>
                <Table.Cell>{new Date(user.createdAt).toLocaleDateString()}</Table.Cell>
                <Table.Cell>
                    
                        <img src={user.profilePicture} alt={user.username} className=" h-20 w-20 rounded object-cover bg-gray-500"/>
                  
                </Table.Cell>
                <Table.Cell>{user.username}</Table.Cell>
                <Table.Cell>{user.email}</Table.Cell>
                <Table.Cell>{user.isAdmin === true ? (<FaCheck className="text-green-500 "/>) : (<FaTimes className= "text-red-500" />)}</Table.Cell>
                <Table.Cell><span onClick={()=> {setShowModal(true); setuserIdToDelete(user._id);}} className="text-red-500 hover:underline cursor-pointer">Delete</span></Table.Cell>
                
            </Table.Row>
            </Table.Body>
        ))}
       
    </Table>
    {showMore && (<button onClick={handleShowMore} className="w-full text-teal-500 self-center text-sm py-7">Show More</button>)}
    </> ) : <p>You have no users yet</p>}
    <Modal show={showModal} onClose={() => setShowModal(false)} popup size="md">
            <Modal.Header></Modal.Header>
            <Modal.Body>
                <div className='text-center'>
                    <HiOutlineExclamationCircle className='h-14 w-14 text-gray-400 dark:text-gray-200 mb-4 mx-auto'/>
                    <h3 className='mb-5 text-lg text-gray-500 dark:text-gray-400'>Are you sure you want to delete this user?</h3>
                    <div className='flex justify-center gap-4'>
                        <Button color='failure' onClick={handleDeleteUser}> Yes, Iam sure </Button>
                        <Button color='gray' onClick={() => setShowModal(false)} > No, Cancel it </Button>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    </div>

    )
}