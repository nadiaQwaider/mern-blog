import React from 'react';
import { Footer } from 'flowbite-react';
import { Link } from 'react-router-dom';
import {BsFacebook, BsInstagram, BsGithub} from 'react-icons/bs'
import logo from '../assets/logo.png';

export default function FooterComponent() {
  return (
    <Footer container className='border border-t-8 border-teal-500'>
        <div className='w-full max-w-7xl mx-auto'>
            <div className='grid w-full justify-between sm:flex md:grid-cols-1'>
              <div className='mt-5'>
              <Link to = "/" className=''>
             <img src={logo} className='h-20 px-2 py-1 rounded-full'/>
        </Link>
              </div>
               
                <div className='grid grid-cols-2 gap-8 sm: mt-4 sm:grid-cols-3 sm:gap-6'>

                  <div>
                    <Footer.Title title='About' />
                      <Footer.LinkGroup col>
                          <Footer.Link href='/about' >
                            About us
                          </Footer.Link>
                          <Footer.Link href='/projects'>
                            Our Projects
                          </Footer.Link>
                      </Footer.LinkGroup>
                  </div>
      
                  <div>
                    <Footer.Title title='Follow us' />
                      <Footer.LinkGroup col>
                          <Footer.Link href='#' target='_blank' rel='noopener noreferrer'>
                            Git
                          </Footer.Link>
                          <Footer.Link href='#' target='_blank' rel='noopener noreferrer'>
                            Social Media
                          </Footer.Link>
                      </Footer.LinkGroup>
                  </div>

                  <div>
                    <Footer.Title title='Legal' />
                      <Footer.LinkGroup col>
                      <Footer.Link href='#' >
                            Privacy &amp; Policy
                          </Footer.Link>
                          <Footer.Link href='#' >
                            Terms &amp; Conditions
                          </Footer.Link>
                      </Footer.LinkGroup>
                  </div>
                </div> 
            </div>
            <Footer.Divider/>
            <div className='w-full sm:flex sm:items-center sm:justify-between'>
              <Footer.Copyright href='#' by="Nadia's Blog" year={new Date().getFullYear()}/>
              <div className='flex gap-6 sm:mt-1 mt-4 sm:justify-center'>
                <Footer.Icon href='#' icon={BsFacebook}/>
                <Footer.Icon href='#' icon={BsInstagram}/>
                <Footer.Icon href='#' icon={BsGithub}/>
              </div>
            </div>
        </div>
    </Footer>
  )
}
