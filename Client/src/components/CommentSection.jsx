import React, {useState, useEffect} from 'react'
import { useSelector} from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'
import { Textarea, Button, Modal } from 'flowbite-react'
import { HiOutlineExclamationCircle } from 'react-icons/hi'
import Comment from "../components/Comment.jsx"

export default function CommentSection({postId}) {
  const { currentUser } = useSelector(state => state.user);
  const [comment, setComment] = useState('');
  const [commentError, setCommentError] = useState('');
  const [comments, setComments] = useState([]);
  const navigate = useNavigate();
  const [showModal, setShowModal] = useState(false);
  const [commentToDelete, setCommentToDelete] = useState(null);
  const handleSubmit = async (e) => {
    try{
      e.preventDefault();
      if(comment.length > 200) {
        return;
      }
      const res = await fetch('/api/comment/create', {
       method:'POST',
       headers: {'Content-Type': 'application/json'},
       body: JSON.stringify({content: comment, postId, userId: currentUser._id}),
      })
      const data = await res.json();
      if(res.ok) {
        setComment('');
        setCommentError(null);
      }
    } catch(error){
      setCommentError(error.message);
    }
     
  }
  useEffect(() => {
  const getComments = async() => {
    try{
     const res = await fetch(`/api/comment/getPostComments/${postId}`);
     if(res.ok){
      const data = await res.json();
      setComments(data);
     }
    }catch(error){
      console.log(error.message)
    }
   }
   getComments()
  },[postId]);

  const handleLike = async(commentId) =>{
   if(!currentUser){
    navigate('/sign-in');
    return;
   }
   const res = await fetch(`/api/comment/likeComment/${commentId}`,{
    method: 'PUT',
   });
   if(res.ok){
    const data = await res.json();
    setComments(comments.map((comment) => 
      comment._id === commentId ? {
        ...comment,
        likes: data.likes,
        numberOfLikes: data.likes.length,
      } : comment
    )
    )}
  }

  const handleDelete = async(commentId) => {
    try{
      setShowModal(false);
     if(!currentUser) {
      navigate('/sign-in');
      return;
     }
     const res = await fetch(`/api/comment/deleteComment/${commentId}`,
      {method: 'DELETE'});
      if (res.ok) {
        const data = await res.json();
        setComments(comments.filter((comment) => comment._id !== commentId))
          }
    }catch(error){
      console.log(error.message)
    }
  }
// after edit any comment, show the comments with the edited one
  const handleEdit = async (comment, editedContent) => {
    setComments( comments.map((s) => comment._id === comment._id ? { ...c, content: editedContent} : c)
  );
};
  return (
    <div>
     { currentUser ?
      (<div>
        <div className='flex items-center gap-1 my-5 text-gray-500'>
          <p>Signed in as:</p> 
          <img className='h-5 w-5 object-cover rounded-full' src = {currentUser.profilePicture} />
          <Link to = '/dashboard?tab=profile' className='text-sm text-teal-500 hover:underline'>@{currentUser.username}</Link>
        </div>
        <form onSubmit={handleSubmit} className='border border-teal-500 p-5 my-5 '>
              <Textarea placeholder='Add a comment...' rows='3' cols='200' maxLength= '200' onChange={(e) => setComment(e.target.value)} value={comment}/>
              <div className='flex justify-between items-center mt-5'>
                <p className='text-xs '>{200 - comment.length} characters remaining</p>
                <Button outline gradientDueTone='PurpleToBlue' type='submit'>Submit</Button>
              </div>
        </form>
        {comments.length === 0 ? (
          <p className='text-sm my-5'>No Comments Yet</p>
        ):(
          <>
          <div className='text-sm my-5'>
            <p>Comments: {comments.length}</p>
          </div>
          {comments.map(comment => (<Comment key={comment.id} comment= {comment} 
                                              onLike={handleLike} 
                                              onEdit={handleEdit} 
                                              onDelete={(commentId)=> 
                                                {
                                                  setShowModal(true)
                                                  setCommentToDelete(commentId)

                                          }}/>))}
          </>
        )}
        
    </div>):(<div className='items-center gap-2 my-5'>
            <span className='text-sm text-teal-500 my-5'> You must sign in if you want to write a comment </span>  
            <Link to='/sign-in' className= 'text-sm text-blue-500 hover:underline'> Sign in </Link>
        </div>
      )
    }
    <Modal show={showModal} onClose={() => setShowModal(false)} popup size="md">
            <Modal.Header></Modal.Header>
            <Modal.Body>
                <div className='text-center'>
                    <HiOutlineExclamationCircle className='h-14 w-14 text-gray-400 dark:text-gray-200 mb-4 mx-auto'/>
                    <h3 className='mb-5 text-lg text-gray-500 dark:text-gray-400'>Are you sure you want to delete this comment?</h3>
                    <div className='flex justify-center gap-4'>
                        <Button color='failure' onClick={()=> { handleDelete(commentToDelete)}}> Yes, Iam sure </Button>
                        <Button color='gray' onClick={() => setShowModal(false)} > No, Cancel it </Button>
                    </div>
                </div>
            </Modal.Body>
    </Modal>
    </div>
  )
}