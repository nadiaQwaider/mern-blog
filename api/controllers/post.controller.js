import { errorHandler } from "../utils/error.js"
import Post from "../models/post.model.js"

export const Create = async (req,res,next) => {
    if(req.user.isAdmin === false){
        return next(errorHandler(403, 'You are not allowed to create a post'))
    }
    if(!req.body.title || !req.body.content){
        return next(errorHandler(403, 'Please fill all required fields'))
    }
    const slug = req.body.title.split(' ').join('').toLowerCase().replace(/[^a-zA-Z0-9-]/,'-');
    const newPost = new Post({
        ...req.body,
        slug,
        userId: req.user.id,
    })
    try{
    const savedPost =await newPost.save();
    res.status(201).json(savedPost);
    }catch(error){
        next(error)
    }
}

export const getPosts = async (req,res,next) => {
    try{
      // startIndex define the number which we will start count with
      const startIndex = parseInt(req.query.startIndex) || 0;
      //the maximum number of posts in the page is 9
      const limit = parseInt(req.query.limit) || 9;
      //sort the posts asc or desc
      const sortDirection = req.query.order === 'asc' ? 1 : -1;
      //posts function
      const posts = await Post.find({
        //search about posts by post author
        ...(req.query.userId && {userId: req.query.userId }),
        ...(req.query.category && {category: req.query.category }),
        ...(req.query.slug && {slug: req.query.slug }),
        ...(req.query.postId && {_id: req.query.postId }),
        //search by title or content of the post
        ...(req.query.searchTerm && {
            $or: [
                {title: {$regex: req.query.searchTerm, $options: 'i'}}, // 'i' means capital and small letter
                {content: {$regex: req.query.searchTerm, $options: 'i'}},
            ],
        }),
    }).sort({ updatedAt: sortDirection }).skip(startIndex).limit(limit);
    //function to count the total number of posts
     const totalPosts = await Post.countDocuments();

    // function to count the posts in the last month (since month ago)
     const now = new Date();
     const oneMonthAgo = new Date(
        now.getFullYear(),now.getMonth() -1, now.getDate()
     );
     const lastMonthPosts = await Post.countDocuments({
        createdAt: {$gte: oneMonthAgo} //return the number of posts where the created date greater than  oneMonthAgo Date
     })
     res.status(200).json({posts, totalPosts, lastMonthPosts});

    } catch(error) {
        next(error);
    }
};

export const deletePost = async(req, res, next) => {
if(req.user.isAdmin === 'false' || req.user.id !== req.params.userId){

    return next(errorHandler(403, 'You are not Allowed to delete this post'));
}
try{
await Post.findByIdAndDelete(req.params.postId);
res.status(200).json('The Post has been deleted');
}catch(error){
    next(error);
}
}

export const updatePost = async (req,res,next) => {
    if(req.user.isAdmin === false || req.user.id !== req.params.userId){
        return next(errorHandler(403, 'You are not allowed to update this post'))
    }
    try{
    const updatedPost =await Post.findByIdAndUpdate(req.params.postId,{
        $set : {
            // only we will allow the user to update these feilds
            title: req.body.title,
            content: req.body.content,
            category: req.body.category,
            image: req.body.image,
        }
    },{new: true})
    res.status(200).json(updatedPost);
    }catch(error){
        next(error)
    }
}

export default Create;