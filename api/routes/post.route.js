import express from 'express';
import { verifyUser} from '../utils/verifyUser.js'
import {Create, deletePost, getPosts, updatePost} from '../controllers/post.controller.js'
const router = express.Router();

router.post('/create', verifyUser, Create);

router.get('/get-posts', getPosts);

router.delete('/delete-post/:postId/:userId', verifyUser, deletePost)

router.put('/update-post/:postId/:userId', verifyUser, updatePost)



export default router;